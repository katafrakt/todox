defmodule Todox.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    ratatouille_opts = [
      app: Todox,
      shutdown: {:application, :todox}
    ]

    children = [
      {Ratatouille.Runtime.Supervisor, runtime: ratatouille_opts}
    ]

    opts = [strategy: :one_for_one, name: Todox.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
