defmodule Todox do
  @behaviour Ratatouille.App

  import Ratatouille.View

  @impl true
  def init(%{window: window}), do: %{window: window}

  @impl true
  def render(model) do
    view do
      row do
        column(size: 6) do
          panel(title: "Tasks", height: :fill) do
          end
        end

        column(size: 6) do
          row do
            column(size: 12) do
              panel(title: "Details", height: model.window.height - 10) do
              end
            end
          end

          row do
            column(size: 12) do
              panel(title: "Help", height: 10)
            end
          end
        end
      end
    end
  end

  @impl true
  def update(model, _message), do: model
end
